var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var router = express.Router();
var session = require('express-session');
var env = require('./api/environment');
var connection = env.Dbconnection;
var app = express();
var multer = require('multer');
var AWS = require('aws-sdk');
var jsonParser = bodyParser.json();
var userApi = require('./api/userApi');


app.use(bodyParser.json());
app.use(cookieParser('brad'));
app.use(session({ resave: false, saveUninitialized: false, secret: 'smith' }));

app.use(bodyParser.urlencoded({ extended: true }));

connection.connect(function(err) {
    if (err) {
        console.log('Error connecting to Db');
        throw err;
    }
    console.log('Connection established');
});


var sessionOptions = {
    secret: "2C44-4D44-WppQ38S",
    resave: true,
    saveUninitialized: true //,
    // cookie: {
    //     secure: false, // Secure is Recommeneded, However it requires an HTTPS enabled website (SSL Certificate)
    //     maxAge: Date.now() + (30 * 86400 * 1000) // 10 Days in miliseconds
    // }
};


app.use(cookieParser());

app.use(session(sessionOptions));

var storage = multer.diskStorage({ // storage code for storing setting
    destination: function(req, file, cb) {
        cb(null, base_urls + 'uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + file.originalname.split('.').pop())
    }
});

var upload = multer({ storage: storage }); 

AWS.config.update({ accessKeyId: env.accessKeyId, secretAccessKey: env.secretAccessKey });


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/api", express.static(path.join(__dirname, 'api')));
app.use(express.static(path.join(__dirname, 'angular6Demo/dist/angular6Demo')));

//server port handles
var server = app.listen(env.port, function() {

    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
});

app.post('/addUser', userApi.addUser);
app.get('/getUser', userApi.getUser);
app.post('/deleteUser', userApi.deleteUser);
app.post('/getUserById', userApi.getUserById);
app.post('/updateUser', userApi.updateUser);