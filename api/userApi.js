var express = require('express');
var http = require('http');
var env = require('./environment');
var connection = env.Dbconnection;
var md5 = require('md5');
var async = require('async');


exports.addUser = function(req, resp, next) {
    connection.query("insert into user (first_name, last_name, email, password) values ('" + req.body.first_name + "', '" + req.body.first_name + "', '" + req.body.email + "', '" + md5(req.body.password) + "')", function(err, rows) {
        if (err) {
            console.log(err);
            resp.status(200).json({ "status": "failed" });
        } else {
            resp.status(200).json({ "status": "success" });
        }
    })
}

exports.updateUser = function(req, resp, next) {
    connection.query("update user set first_name = '" + req.body.first_name + "', last_name = '" + req.body.first_name + "', email = '" + req.body.email + "'", function(err, rows) {
        if (err) {
            console.log(err);
            resp.status(200).json({ "status": "failed" });
        } else {
            resp.status(200).json({ "status": "success" });
        }
    })
}

exports.getUser = function(req, resp, next) {
    connection.query("select * from user", function(err, rows) {
        if (err) {
            console.log(err);
            resp.status(200).json({ "status": "failed" });
        } else {
            resp.status(200).json({ "status": "success", "data": rows });
        }
    })
}
exports.deleteUser = function(req, resp, next) {
    connection.query("delete from user where userId = " + req.body.userId + "", function(err, rows) {
        if (err) {
            console.log(err);
            resp.status(200).json({ "status": "failed" });
        } else {
            resp.status(200).json({ "status": "success" });
        }
    })
}

exports.getUserById = function(req, resp, next) {
    connection.query("select * from user where userId = " + req.body.userId + "", function(err, rows) {
        if (err) {
            console.log(err);
            resp.status(200).json({ "status": "failed" });
        } else {
            resp.status(200).json({ "status": "success", "data": rows[0] });
        }
    })
}